extends KinematicBody



const NORTH = Vector3(0.0, 1.0, -1.0)
const EAST = Vector3(1.0, 0.0, 0.0)
const SOUTH = Vector3(0.0, 0.0,1.0)
const WEST = Vector3(-1.0, 0.0,0.0)

var xdelta = Vector3()
var zdelta = Vector3()
var time = 0
var Amplitude = 0.125
onready var w = get_node("/root/water")

onready var front = get_node("front")
onready var back = get_node("back")

onready var baum = get_node("Scene Root/baum")
onready var grosssegel = get_node("Scene Root/baum/origin")


onready var wind_arrow = get_node("Camera/HUD/wind_dir/wind_arrow")

var wind_speed = 1
var wind_dir = Vector3(1.0, 0.0, 0.0) #wind_dir is a normalized Direction Vector to which the Wind is going _NOT_ Where it comes from
const SPEED_GRADIENT = -0.64 # m for the speed function m*wind_dir + 2
const DRAG_FACTOR = 0.5

var baum_rot_weight = 0.1

func _ready():
	set_physics_process(false)

func _physics_process(delta):
	var boat_normal = (get_node("front").get_global_transform().origin - get_node("back").get_global_transform().origin).normalized()
	var wind_angle = wind_dir.angle_to(NORTH)
	#wind_arrow.set_rotation(wind_angle)

	#var gross_normal = baum.get_node("baum_2").get_global_transform().origin - baum.get_node("baum_1").get_global_transform().origin

	#var wind_rel_dir = gross_normal.angle_to(wind_dir)
	#var gross_speed = (SPEED_GRADIENT*abs(wind_rel_dir) + 2) * wind_speed
	#var wind_drag = wind_dir * wind_speed * DRAG_FACTOR
	#var boat_velocity = wind_drag + boat_normal * wind_speed 



	time += delta
	var height = w.wave_formula(get_translation().x, get_translation().z, time)
	set_translation(Vector3(get_translation().x, height, get_translation().z))
	

	
	var rightheight = sin(Amplitude * get_node("right").get_global_transform().origin.z - time)
	var leftheight = w.wave_formula(get_node("left").get_global_transform().origin.x, get_node("left").get_global_transform().origin.z, time)
	xdelta.y = rightheight - leftheight

	var frontheight = w.wave_formula(get_node("left").get_global_transform().origin.x, get_node("front").get_global_transform().origin.z, time)
	var backheight = w.wave_formula(get_node("left").get_global_transform().origin.x, get_node("back").get_global_transform().origin.z, time)
	zdelta.y = backheight - frontheight

	set_rotation(Vector3(xdelta.y, get_rotation().y, get_rotation().z))
	
	
	
	var front_point = front.get_global_transform().origin.normalized()
	var back_point = back.get_global_transform().origin.normalized()
	var wind_point = (wind_dir + get_global_transform().origin).normalized()
	var wind_side = sign((back_point.x - front_point.x) * (wind_point.y - front_point.y) - (back_point.y - front_point.y) * (wind_point.x - front_point.x))
	print(wind_side)

	if Input.is_key_pressed(KEY_A):
		if wind_side == 1:
			baum.rotate_y(-0.005*(baum.get_rotation().y*1.5))
		else:
			baum.rotate_y(-0.005*(abs(baum.get_rotation().y) + 1))
	if Input.is_key_pressed(KEY_D):
		if wind_side == 1:
			baum.rotate_y(0.005*(baum.get_rotation().y + 1))
		else:
			baum.rotate_y(-0.005*(baum.get_rotation().y*1.25))


	

