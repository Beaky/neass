extends MeshInstance

# class member variables go here, for example:
# var a = 2
var t = 0

onready var w = get_node("/root/water")

func _ready():
	set_physics_process(true)

func _physics_process(delta):
	t += delta
	#var height = h * sin(A * D.dot(Vector2(get_translation().x, get_translation().z)) * w + t*p)
	#height *= h * sin(A * Di.dot(Vector2(get_translation().x, get_translation().z)) * w + t*p)
	#height *= h * sin(A * Dj.dot(Vector2(get_translation().x, get_translation().z)) * w + t*p)
	var height = w.wave_formula(get_translation().x, get_translation().z, t)
	set_translation(Vector3(get_translation().x, height, get_translation().z))

