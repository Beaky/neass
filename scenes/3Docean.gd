extends Spatial

var wave_particle_res = preload("res://scenes/MeshInstance.tscn")
var z = 0
var x = 0
var i = 1
export var xsize = 20
export var zsize = 100
export var active = true

func _ready():
	if active:
		instance_water()

func instance_water():
	for z in range(0, zsize):
		for x in range(0, xsize):
			var wave_particle = wave_particle_res.instance()
			wave_particle.set_translation(Vector3(x, 0, z))
			self.add_child(wave_particle)
