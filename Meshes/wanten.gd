extends ImmediateGeometry

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var wanten = [[get_node("w1_1").get_translation(), get_node("w1_2").get_translation()], [get_node("w2_1").get_translation(), get_node("w2_2").get_translation()]]
var m = SpatialMaterial.new()

func _ready():

	for want in wanten:
		var im = get_node(".")
		im.set_material_override(m)
		im.clear()
		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		im.add_vertex(want[0])
		im.add_vertex(want[1])
		im.end()