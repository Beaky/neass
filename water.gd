extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var Amplitude = 0.25


func wave_formula(x, z, t):
	var height = sin(Amplitude * z  - t)
	return height